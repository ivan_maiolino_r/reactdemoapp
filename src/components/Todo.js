import React from 'react';
// Import Components
import TodoItem from './TodoItem'


const  TodoList=({todos,setTodos,filteredTodos}) =>{
    console.log(todos);
return(
<div className="todo-container">
      <ul className="todo-list">
          {
              filteredTodos.map((todo) => (
                <TodoItem 
                            text={todo.text} 
                            key={todo.id} 
                            todos={todos} 
                            setTodos = {setTodos} 
                            todo={todo
                }/>
              ))
          }
      </ul>
    </div>
    );
}

export default TodoList;